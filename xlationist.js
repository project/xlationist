
/**
 * Xlationist module for Drupal
 *
 * By Jakob Persson of NodeOne
 */

/**
 * http://brandonaaron.net/blog/2007/06/17/jquery-snippets-outerhtml/
 */
jQuery.fn.outerHTML = function() {
  return $('<div>').append( this.eq(0).clone() ).html();
};

/**
 * Insert content at caret position (converted to jQuery function)
 *
 * http://alexking.org/blog/2003/06/02/inserting-at-the-cursor-using-javascript
 * http://www.mail-archive.com/jquery-en@googlegroups.com/msg08708.html
 */
jQuery.fn.insertAtCaret = function(myValue) {
  return this.each(function(){
    //IE support
    if (document.selection) {
      this.focus();
      sel = document.selection.createRange();
      sel.text = myValue;
      this.focus();
    }
    //MOZILLA/NETSCAPE support
    else if (this.selectionStart || this.selectionStart == '0') {
      var startPos = this.selectionStart;
      var endPos = this.selectionEnd;
      var scrollTop = this.scrollTop;
      this.value = this.value.substring(0, startPos) + myValue + this.value.substring(endPos, this.value.length);
      this.focus();
      this.selectionStart = startPos + myValue.length;
      this.selectionEnd = startPos + myValue.length;
      this.scrollTop = scrollTop;
    } 
    else {
      this.value += myValue;
      this.focus();
    }
  });
};

/**
 * Drupal behavior
 */
Drupal.behaviors.xlationist = function(context) {
  var source_element = $('#locale-translate-edit-form div.form-item:first');

  // Add wrapper tags
  source_element.html(source_element.html().replace(/((!|%|@)\w+)+/gim, function(m) {
    return xlationist.wrap(m)
  }));

  // Add button to insert entire original text
  source_element.after(xlationist.insert_all());

  // Handle clicking placeholders
  $('.xlationist-placeholder').bind('click', function() {
    $('textarea[id^="edit-translations-"]').insertAtCaret($(this).text().replace('+', ''));
  });

  // Handle clicking "insert entire original text"
  $('.xlationist-insertall').bind('click', function() {
    $('textarea[id^="edit-translations-"]').insertAtCaret(Drupal.settings.xlationist.source_text);
  });

};

/**
 * Namespace
 */
xlationist = {

  /**
   * Wrap placeholder
   */
  wrap : function(string) {
    if (typeof string == 'string' && string.length) {
      return($('<span/>')
        .addClass('xlationist-placeholder xlationist-rounded')
        .text(string)
        .prepend($('<span/>')
          .addClass('xlationist-add')
          .append('+'))
        .outerHTML());
    }
  },

  /**
   * Return an "insert all" button
   */
  insert_all : function() {
    return($('<span/>').addClass('xlationist-insertall xlationist-rounded').text(Drupal.t('Insert entire original text')));
  }
};