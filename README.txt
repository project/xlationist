Xlationist Module for Drupal 6.x

Improves the usability of the string translation form in Drupal by making
placeholders clickable and adding other shortcuts.

Author
------
The module was developed by Jakob Persson <http://drupal.org/user/37564> of
NodeOne, Sweden's leading Drupal consulting firm. Our mission is to empower
users by building usable, powerful and effective web solutions for our clients.

Visit us at http://nodeone.se

The author can be contacted for paid customizations of this module as well as
Drupal consulting, installation, development and customizations.

Sponsors
--------
The development of this module has been sponsored by

  * NodeOne <http://nodeone.se>